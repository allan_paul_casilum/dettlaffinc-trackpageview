<?php 
namespace DettlaffincTrackPageView\DettlaffincTrackPageView;

use Illuminate\Support\Facades\Facade;

class TrackPageViewFacade extends Facade{
    protected static function getFacadeAccessor() { return 'TrackPageView'; }
}