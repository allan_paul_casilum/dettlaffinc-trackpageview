<?php

namespace DettlaffincTrackPageView\DettlaffincTrackPageView;

use Illuminate\Support\ServiceProvider;

class TrackPageViewProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
			__DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
		], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('TrackPageView', function() {
            return new TrackPageView;
        });
    }
}
