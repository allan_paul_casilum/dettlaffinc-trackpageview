<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackPageViewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_page_view', function (Blueprint $table) {
            $table->increments('id');
			$table->string('session_id');
			$table->text('user_agent')->nullable();
			$table->string('ip_address', 50)->nullable();
			$table->string('url_path', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_page_view');
    }
}
