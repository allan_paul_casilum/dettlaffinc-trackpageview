<?php

namespace DettlaffincTrackPageView\DettlaffincTrackPageView;

use Illuminate\Database\Eloquent\Model;

class TrackPageView
{
	
	public function test() 
	{
		echo 'hello world';
	}
	
	public function init()
	{
		\DettlaffincTrackPageView\DettlaffincTrackPageView\TrackPageViewModel::create([
			'session_id' => session()->getId(),
			'user_agent' => \Request::header('User-Agent'),
			'ip_address' => \Request::ip(),
			'url_path' => \Request::path(),
		]);
	}
	
    public function __construct(){}
}
