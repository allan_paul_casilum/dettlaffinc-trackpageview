<?php

namespace DettlaffincTrackPageView\DettlaffincTrackPageView;

use Illuminate\Database\Eloquent\Model;

class TrackPageViewModel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'track_page_view';
	
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['session_id', 'user_agent', 'ip_address', 'url_path'];
}
